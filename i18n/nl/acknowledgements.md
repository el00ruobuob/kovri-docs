## Dankwoord
Naast iedereen die heeft meegewerkt aan Boost en OpenSSL, en alle programmeurs die dit project mogelijk hebben gemaakt, willen we de volgende personen in het bijzonder bedanken:

- Wei Dai, Jeffrey Walton en alle ontwikkelaars van Crypto++ voor het cre�ren van een invloedrijke gratis cryptografie-bibliotheek in C++
- Dean Michael Berris, Glyn Matthews, en alle ontwikkelaars van cpp-netlib voor het cre�ren van een actief onderhouden netwerk-bibliotheek voor meerdere platformen
- zzz, str4d en alle huidige en vroegere ontwikkelaars van I2P in Java die de standaard hebben vastgesteld die wordt gevolgd door alle andere I2P-implementaties
- orion voor ```i2pcpp```, de [eerste](http://git.repo.i2p.xyz/w/i2pcpp.git) I2P-implementatie in C++
- orignal voor ```i2pd```, de meest complete I2P-implementatie in C++, [waarvan de onze is afgeleid](https://github.com/purplei2p/i2pd/commit/45d27f8ddc43e220a9eea42de41cb67d5627a7d3)
- EinMByte voor het verbeteren van alle I2P-implementaties in C++ (inclusief Kovri)
