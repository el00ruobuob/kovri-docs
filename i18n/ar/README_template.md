[<img width="300" src="https://static.getmonero.org/images/kovri/logo.png" alt="ˈKoʊvriː" />](https://gitlab.com/kovri-project/kovri)

1. [لتغطية ،حجب ، إلتفاف](https://en.wikipedia.org/wiki/Esperanto)
2. تقنيه إخفاء للهويه مجانيه لا مركزيه تعتمد علي المواصفات المفتوحه لـ [I2P] (https://getmonero.org/resources/moneropedia/i2p.html)

## إخلاء مسؤولية
- حالياً البرنامج في مرحله **ألفا** تحت التطوير ( لم يتم دمجه مع مونيرو بعد )

## بدايه سريعه

- تريد ثنائيات بنيت مسبقا؟ [تنزيل أدناه] (#Downloads)
- هل ترغب في البناء والتثبيت بنفسك؟ [تعليمات البناء أدناه](#building)

## إقرأني متعدد اللغات
هذه الصفحه متاحه أيضاً باللغات التاليه :

- [إيطالي](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/it/README.md)
- أسباني
- روسي
- [فرنسي](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/fr/README.md)
- [ألماني](https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/de/README.md)
- دانماركي

## التنزيلات

### الإصدارات

قريباً[tm]

### [الإصدارات التجريبيه (تجريبي)]()

قريباً[tm]

## التغطيه

| Type      | Status |
|-----------|--------|
| Coverity  | [![Coverity Status](https://scan.coverity.com/projects/7621/badge.svg)](https://scan.coverity.com/projects/7621/)
| Codecov   | [![Codecov](https://codecov.io/gl/kovri-project/kovri/branch/master/graph/badge.svg)](https://codecov.io/gl/kovri-project/kovri)
| License   | [![License](https://img.shields.io/badge/license-BSD3-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

## البناء

### التبعيات والبيئة

| Dependency          | Minimum version              | Optional | Arch Linux  | Ubuntu/Debian    | macOS (Homebrew) | FreeBSD       | OpenBSD     |
| ------------------- | ---------------------------- |:--------:| ----------- | ---------------- | ---------------- | ------------- | ----------- |
| git                 | 1.9.1                        |          | git         | git              | git              | git           | git         |
| gcc                 | 4.9.2                        |          | gcc         | gcc              |                  |               |             |
| clang               | 3.5 (3.6 on FreeBSD)         |          | clang       | clang            | clang (Apple)    | clang36       | llvm        |
| CMake               | 3.5.1                        |          | cmake       | cmake            | cmake            | cmake         | cmake       |
| gmake (BSD)         | 4.2.1                        |          |             |                  |                  | gmake         | gmake       |
| Boost               | 1.58                         |          | boost       | libboost-all-dev | boost            | boost-libs    | boost       |
| OpenSSL             | Always latest stable version |          | openssl     | libssl-dev       | openssl          | openssl       | openssl     |
| Doxygen             | 1.8.6                        |    X     | doxygen     | doxygen          | doxygen          | doxygen       | doxygen     |
| Graphviz            | 2.36                         |    X     | graphviz    | graphviz         | graphviz         | graphviz      | graphviz    |
| Docker              | Always latest stable version |    X     | See website | See website      | See website      | See website   | See website |

#### Windows (MSYS2/MinGW-64)
* قم بتنزيل [MSYS2 installer](http://msys2.github.io/), 64-bit or 32-bit as needed
* استخدم الاختصار المرتبط بالنسخه الخاصة بك لتشغيل بيئة MSYS2. على أنظمة 64 بت الاختصار هو `MinGW-w64 Win64 Shell`. ملاحظة: إذا كنت تستخدم نظام التشغيل 64 بت من ويندوز ، فستكون لديك بيئات 64 بت و 32 بت
* تحديث الحزم في تثبيت MSYS2 الخاص بك:

```shell
$ pacman -Sy
$ pacman -Su --ignoregroup base
$ pacman -Syu
```

#### تنصيب الحزم

ملاحظه: لنسخه i686 , قم بتبديل `mingw-w64-x86_64` بـ `mingw-w64-i686`

`$ pacman -S make mingw-w64-x86_64-cmake mingw-w64-x86_64-gcc mingw-w64-x86_64-boost mingw-w64-x86_64-openssl`

إختياري:

`$ pacman -S mingw-w64-x86_64-doxygen mingw-w64-x86_64-graphviz`

### أنشيء ونصّب

**لا تستخدم الملف المضغوط من gitlab قم بإستنساخ فقط**

```bash
$ git clone --recursive https://gitlab.com/kovri-project/kovri
$ cd kovri && make release  # see the Makefile for all build options
$ make install
```

- يجب على المستخدمين النهائيين تشغيل `make install` للتثبيت من جديد
- يجب على المطورين تشغيل `make install` بعد إنشاء حديث

### Docker

أو بناء محليا مع Docker

```bash
$ docker build -t kovri:latest .
```

## التوثيق والتطوير
- يتوفر [دليل المستخدم] (https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/ar/user_guide.md) لجميع المستخدمين
- يتوفر [دليل المطور] (https://gitlab.com/kovri-project/kovri-docs/blob/master/i18n/en/developer_guide.md) للمطورين (يرجى القراءة قبل فتح طلب السحب)
- يمكن العثور على المزيد من الوثائق بلغتك المفضلة في مستودع [kovri-docs] (https://gitlab.com/kovri-project/kovri-docs/)
- [مونيروبيديا] (https://getmonero.org/resources/moneropedia/kovri.html) موصى به لجميع المستخدمين والمطورين
- [نظام تمويل المنتدي] (https://forum.getmonero.org/8/funding-required) للحصول على تمويل من أجل عملك ، [تقديم اقتراح] (https://forum.getmonero.org/7/open-tasks/2379/forum-funding-system-ffs-sticky)
- [repo.getmonero.org] (https://repo.getmonero.org/monero-project/kovri) أو monero-repo.i2p بدائل لـ GitLab للوصول إلى مستودع التخزين غير المضغوط
- انظر أيضا [موقع kovri] (https://gitlab.com/kovri-project/kovri-site) و [monero / kovri meta] (https://github.com/monero-project/meta)

## استجابه للثغرات
- تشجع [سياسه الإستجابه للثغرات] (https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) على الإفصاح المسؤول
- متاح أيضاً من خلال [HackerOne](https://hackerone.com/monero)

## الإتصال والمساعده
- IRC: [Freenode](https://webchat.freenode.net/) | Irc2P with Kovri
  - `#kovri` | المجتمع && الإتصال والمساعده
  - `#kovri-dev` | قناه التطوير
- Twitter: [@getkovri](https://twitter.com/getkovri)
- بريد:
  - anonimal [at] getmonero.org
  - PGP Key fingerprint: 1218 6272 CD48 E253 9E2D  D29B 66A7 6ECF 9144 09F1
- [Kovri Slack](https://kovri-project.slack.com/) (الدعوه مطلوبه من خلال البريد او IRC)
- [Monero Mattermost](https://mattermost.getmonero.org/)
- [Monero StackExchange](https://monero.stackexchange.com/)
- [Reddit /r/Kovri](https://www.reddit.com/r/Kovri/)

## Donations
- قم بزياره [صفحه التبرعات](https://getmonero.org/getting-started/donate/) لمساعده كوفري بتبرعك
